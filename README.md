# Demo for uruburu (UI Widgets for real time data)

Mqtt bridge example for [uruburu](https://github.com/vandrito/uruburu) library.

![Screen capture](https://github.com/vandrito/uruburu/blob/master/assets/uruburu.png)


## Install

### Prerequisits

You need _bower_ and _node_ to be installed.

### How to

Clone the repository to your local workspace and then, inside the newely created folder, do:

    npm install

## Run examples

Run, for example, [mosquitto](http://mosquitto.org/) as mqtt broker.  
This is necessary, since the data server used is in fact just a simple bridge to mqtt brokers.

To run server part:

    cd test
    node server.js

To run client part (simple data generation):

    cd test
    node client.js
  
## Uruburu library usage

See [views/index.jade](https://github.com/vandrito/uruburu_mqtt/blob/master/views/index.jade) or [public/index.html](https://github.com/vandrito/uruburu_mqtt/blob/master/public/index.html) files.

## Which dependencies and why ?

* mqtt.js: mqtt client side, mqtt implementation for Mosca server.
* express.js and related middlewares (_body-parser_, _serve-favicon_): provide a way to create a web server with some sugar embedded and with only few lines of code
* jade: because I don't like HTML and I prefer Jade syntax. Of course, these can be removed very simply and quickly
* my-ip: get the ip of the machine in a quite clean way. No need to hardcode the ip.
